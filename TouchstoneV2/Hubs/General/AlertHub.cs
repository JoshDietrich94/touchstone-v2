﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace TouchstoneV2.Hubs.General
{
    public class AlertHub : Hub
    {
        public async Task SendMessage(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", message);
        }
    }
}
