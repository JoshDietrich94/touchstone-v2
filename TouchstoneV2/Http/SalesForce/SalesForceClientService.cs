﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TouchstoneV2.Constants;
using TouchstoneV2.Models.SalesForce;

namespace TouchstoneV2.Http.SalesForce
{
    /// <summary>
    /// Sales Force C---- operation access.
    /// </summary>
    public class SalesForceClientService : ISalesForceClientService
    {
        #region Request

        public async Task<string> PostAsync<T>(string uri, T data)
        {
            try
            {
                var httpClient = CreateHttpClient();

                string jsonResult = string.Empty;

                var responseMessage = await Policy
                    .Handle<WebException>(ex =>
                    {
                        return true;
                    })
                    .OrResult<HttpResponseMessage>(ex =>
                    {
                        if (ex.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            this.RefreshTokenAsync().GetAwaiter().GetResult();
                            httpClient = CreateHttpClient();

                            return true;
                        }

                        return false;
                    })
                    .WaitAndRetryAsync
                    (
                        2,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(0, retryAttempt))
                    )
                    .ExecuteAsync(async () => await httpClient.PostAsync(uri, GetHttpContent(data)));

                jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (responseMessage.IsSuccessStatusCode)
                {
                    // response is not in json
                    //var json = JsonConvert.DeserializeObject<Response>(jsonResult);

                    return jsonResult;
                }

                throw new Exception(jsonResult);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        #endregion Request

        #region Refresh Request
        private async Task RefreshTokenAsync()
        {
            try
            {
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded;");

                var clientId = AppSettings.SalesForce.ClientID;
                var refreshToken = AppSettings.SalesForce.RefreshToken;

                var req = new HttpRequestMessage(HttpMethod.Post, ApiConstants.SALES_FORCE_AUTHORIZE_ACCESS_TOKEN_URL);
                req.Content = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "grant_type", ApiConstants.GRANT_TYPE_REFRESH },
                    { "client_id", clientId },
                    { "refresh_token", refreshToken },
                });

                string jsonResult = string.Empty;

                var responseMessage = await Policy
                    .Handle<WebException>(ex =>
                    {
                        return true;
                    })
                    .WaitAndRetryAsync
                    (
                        2,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(0, retryAttempt))
                    )
                    .ExecuteAsync(async () => await httpClient.SendAsync(req).ConfigureAwait(false));

                if (responseMessage.IsSuccessStatusCode)
                {
                    jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                    var json = JsonConvert.DeserializeObject<Token>(jsonResult);

                    if (string.IsNullOrWhiteSpace(json?.AccessToken))
                        throw new Exception($"Response resulted in a null refresh token. \n Response: {json}");
                    else
                        SFIdentity.AuthToken = json.AccessToken;
                }
                else
                {
                    var ehre = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Refresh token error {e.Message}");
            }
        }

        #endregion Refresh Request

        #region Init Functions

        private StringContent GetHttpContent<T>(T data)
        {
            var content = new StringContent(JsonConvert.SerializeObject(data));

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return content;
        }

        private HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", SFIdentity.AuthToken);

            httpClient.Timeout = TimeSpan.FromSeconds(300);

            return httpClient;
        }

        #endregion Init Functions
    }
}
