﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TouchstoneV2.Http.SalesForce
{
    public interface ISalesForceClientService
    {
        Task<string> PostAsync<T>(string uri, T data);
    }
}
