﻿using System.Threading.Tasks;

namespace TouchstoneV2.Http
{
    public interface IHttpClientService
    {
        Task<T> GetAsync<T>(string uri);
        TR PostAsync<T, TR>(string uri, T data);
    }
}