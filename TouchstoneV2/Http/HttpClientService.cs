﻿using Newtonsoft.Json;
using Polly;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TouchstoneV2.Http
{
    public class HttpClientService : IHttpClientService
    {
        #region ctor

        public HttpClientService()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chaine, sslPolicyErrors) => true;
        }

        #endregion

        #region HTTP Request

        public async Task<T> GetAsync<T>(string uri)
        {
            try
            {
                var httpClient = CreateHttpClient();

                string jsonResult = string.Empty;

                var responseMessage = await Policy
                    .Handle<WebException>(ex =>
                    {
                        Debug.WriteLine($"{ex.GetType().Name} : {ex.Message}");
                        return true;
                    })
                    .WaitAndRetryAsync
                    (
                        5,
                        retryAttempt => TimeSpan.FromSeconds(Math.Pow(0, retryAttempt))
                    )
                    #pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
                    .ExecuteAsync(async () => httpClient.GetAsync(uri).ConfigureAwait(false).GetAwaiter().GetResult());
                    #pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously

                if (responseMessage.IsSuccessStatusCode)
                {
                    jsonResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                    var json = JsonConvert.DeserializeObject<T>(jsonResult);

                    return json;
                }

                throw new WebException(responseMessage.ReasonPhrase);
            }
            catch (Exception e)
            {
                Debug.WriteLine($"{e.GetType().Name} : {e.Message}");

                throw new HttpRequestException(e.Message);
            }
        }

        public TR PostAsync<T, TR>(string uri, T data)
        {
            try
            {
                var httpClient = CreateHttpClient();

                var content = new StringContent(JsonConvert.SerializeObject(data));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string jsonResult = string.Empty;

                var responseMessage = httpClient.PostAsync(uri, content).GetAwaiter().GetResult();

                jsonResult = responseMessage.Content.ReadAsStringAsync().Result;

                if (responseMessage.IsSuccessStatusCode)
                    return JsonConvert.DeserializeObject<TR>(jsonResult);

                throw new Exception(jsonResult);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "\n \nStrackTrace: \n" + ex.StackTrace);
            }
        }

        #endregion

        #region Create HTTP Client

        private HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Clear();

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }

        #endregion
    }
}