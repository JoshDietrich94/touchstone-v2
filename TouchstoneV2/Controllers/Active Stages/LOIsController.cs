﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouchstoneV2.DTO.IndexViews;
using TouchstoneV2.Models.TouchstoneContext;

namespace TouchstoneV2.Controllers
{
    public class LOIsController : RootController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CreateLead()
        {
            var PartnerID = HttpContext.Session.GetString("PartnerID");

            using TouchstoneV2Context _db = new TouchstoneV2Context();
            Leads newLead = new Leads
            {
                ID = Guid.NewGuid().ToString(),
                PartnerID = PartnerID
            };
            _db.Leads.Add(newLead);
            _db.SaveChanges();

            return RedirectToAction("Edit", new { newLead.ID });
        }

        [HttpPost]
        public JsonResult GetLOIs()
        {
            var PartnerID = HttpContext.Session.GetString("PartnerID");

            using TouchstoneV2Context _db = new TouchstoneV2Context();
            var Leads = _db.Leads.AsNoTracking()
                .Include(x => x.Customers)
                .Include(x => x.Properties)
                .Include(x => x.MainStage)
                .ToList()
                .Where(x => x.PartnerID == PartnerID && x.MainStage.Label == "LOIs")
                .Select(x =>
                {
                    var Customer = x.Customers.FirstOrDefault(x => x.ContactRole == "1");
                    var Property = x.Properties.FirstOrDefault();
                    return new LOIs
                    {
                        ID = x.ID,
                        CreationStampDT = x.CreationStamp,
                        CustomerName = Customer == null ? "" : 
                                       Customer.VerifiedFirstName == null ?
                                    $"{Customer.LoiFirstName} {Customer.LoiLastName}" :
                                    $"{Customer.VerifiedFirstName} {Customer.VerifiedLastName}",
                        Address = Property == null ? "" : $"{Property.Address}, {Property.City}, {Property.State} {Property.Zip}",
                        Phone = Customer == null ? "" : Customer?.Phone
                    };
                })
                .ToList();

            return Json(Leads);
        }
    }
}
