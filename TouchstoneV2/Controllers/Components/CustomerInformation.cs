﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouchstoneV2.Models.TouchstoneContext;
using TouchstoneV2.ViewComponents.Models;

namespace TouchstoneV2.ViewComponents
{
    [ViewComponent]
    public class CustomerInformation : ViewComponent
    {
        private readonly TouchstoneV2Context _db;

        public CustomerInformation()
        {
            _db = new TouchstoneV2Context();
        }

        public async Task<IViewComponentResult> InvokeAsync(string ID, Leads Lead)
        {
            CustomerInformation_VM Page = new CustomerInformation_VM
            {
                PageData = new CustomerPageData
                {
                    ID = ID,
                    Lead = Lead
                }
            };

            await Task.FromResult(true);
            return View(Page);
        }
    }
}

namespace TouchstoneV2.ViewComponents.Models
{
    public class CustomerInformation_VM
    {
        public CustomerPageData PageData { get; set; }
    }

    public class CustomerPageData : PageData
    {
        // For future use, if Customer needs information beyond the default PageData
    }
}