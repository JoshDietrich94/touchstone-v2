﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouchstoneV2.Models.TouchstoneContext;
using TouchstoneV2.ViewComponents.Models;

namespace TouchstoneV2.Controllers
{
    public class RootController : Controller
    {
        public IActionResult Edit(string ID)
        {
            var PartnerID = HttpContext.Session.GetString("PartnerID");

            using TouchstoneV2Context _db = new TouchstoneV2Context();
            var Lead = _db.Leads
                .Include(x => x.Customers)
                .Include(x => x.Properties)
                .FirstOrDefault(x => x.ID == ID);
            if (Lead == null)
                throw new ArgumentException("Invalid Lead ID");

            PageData Page = new PageData
            {
                ID = ID,
                Lead = Lead
            };

            return View(Page);
        }
    }
}

namespace TouchstoneV2.ViewComponents.Models
{
    public class PageData
    {
        public string ID { get; set;}
        public Leads Lead { get; set; }
    }
}