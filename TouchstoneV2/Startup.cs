using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Rotativa.AspNetCore;
using TouchstoneV2.Extensions.Infrastructure;

namespace TouchstoneV2
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment _env)
        {
            Configuration = configuration;
            Env = _env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.Configure<FormOptions>(options =>
            {
                options.ValueCountLimit = 16384;
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = long.MaxValue;
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddMvc();
            services.AddSignalR();
            services.AddMemoryCache();
            services.AddDistributedSqlServerCache(options =>
            {
                options.ConnectionString = @"Data Source=192.168.0.20;Initial Catalog=TSSessions;User ID=sa;Password=MITTENS1;Max Pool Size=500;Min Pool Size=15;Pooling=true;";
                options.SchemaName = "dbo";
                options.TableName = "Sessions";
                options.DefaultSlidingExpiration = TimeSpan.FromDays(10);
            });
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.Secure = CookieSecurePolicy.None;
                options.HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.None;
            });
            services.AddSession(options =>
            {
                options.Cookie.MaxAge = TimeSpan.FromDays(10);
                options.IOTimeout = TimeSpan.FromDays(10);
                options.Cookie.Name = "Touchstone.Session";
                options.IdleTimeout = TimeSpan.FromDays(10);
                options.Cookie.IsEssential = true;
            });

            services.AddTouchstoneServices();

            var builder = services.AddControllersWithViews().AddJsonOptions(x => x.JsonSerializerOptions.PropertyNamingPolicy = null);
            if (Env.IsDevelopment() == true)
            {
                builder.AddRazorRuntimeCompilation();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, Microsoft.AspNetCore.Hosting.IHostingEnvironment env2)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseStaticFiles();

            //app.UseHttpContextItemsMiddleware();

            app.UseRouting();

            app.UseAuthorization();
            app.UseSession();
            app.Use(async (context, next) =>
            {

                context.Features.Get<IHttpMaxRequestBodySizeFeature>().MaxRequestBodySize = null;

                //if(context.Request.Path)
                //Debug.WriteLine(context.Request.Scheme);
                //Debug.WriteLine(context.Request.Host);
                string lcReqPath = context.Request.Path.ToString().ToLower();
                Debug.WriteLine(lcReqPath);
                var curSession = context.Session;
                if (curSession == null || string.IsNullOrWhiteSpace(curSession.GetString("UserID")))
                {
                    //path needs to be in lower case for it to not redirect to loginpage
                    if (!lcReqPath.Contains("/managesettings/m1fixemail")
                        && !lcReqPath.Contains("/managesettings/generatecsv")
                        && !lcReqPath.Contains("/corelogic")
                        && !lcReqPath.Contains("getimage")
                        && !lcReqPath.Contains("/login")
                        && !lcReqPath.Contains("/script")
                        && !lcReqPath.Contains("/customer")
                        && !lcReqPath.Contains("api")
                        && !lcReqPath.Contains("/loiform/edit")
                        && !lcReqPath.Contains("/login/updatepassword")
                        && !lcReqPath.Contains("/login/forgotpassword")
                        && !lcReqPath.Contains("/login/changepassword")
                        && !lcReqPath.Contains("/login/changepasswordlink")
                        && !lcReqPath.Contains("/scheduler/deactivator")
                        && !lcReqPath.Contains("/payroll/pdfgeneration")
                        && !lcReqPath.Contains("/payroll/dealerorfranchisepdf")
                        && !lcReqPath.Contains("emailinsidesalepaymentpdf")
                        && !lcReqPath.Contains("/payroll/insidesalerepreport")
                        && !lcReqPath.Contains("/sunnova/oauth_callback")
                        && !lcReqPath.Contains("/proposals/proposallist")
                        && !lcReqPath.Contains("/proposaltool/cashcontract")
                        && !lcReqPath.Contains("/proposaltool/createproposalpdf"))
                    {
                        context.Response.Redirect("/Login");
                        return;
                    }
                }
                else
                {
                    //Debug.WriteLine(curSession.Id);
                    curSession.SetString("LastAction", DateTime.Now.ToString());
                    //Sales reps are only allowed to access these controllers/views/functions.
                    if (Convert.ToString(curSession.GetString("SalesRepFlag")) == "True" && Convert.ToString(curSession.GetString("InsideSales")) == "False" && Convert.ToString(curSession.GetString("PayrollReport")) == "False")
                    {
                        if (!lcReqPath.Contains("rep")
                        && !lcReqPath.Contains("/mydeals")
                        && !lcReqPath.Contains("/infystore")
                        && !lcReqPath.Contains("getimage")
                        && !lcReqPath.Contains("dynamic_file_upload")
                        && !lcReqPath.Contains("home")
                        && !lcReqPath.Contains("lead")
                        && !lcReqPath.Contains("populate")
                        && !lcReqPath.Contains("scheduledinstall")
                        && !lcReqPath.Contains("salesoverview")
                        && !lcReqPath.Contains("fulljobdetail")
                        && !lcReqPath.Contains("training")
                        && !lcReqPath.Contains("infyboard")
                        && !lcReqPath.Contains("infycoin")
                        && !lcReqPath.Contains("async")
                        && !lcReqPath.Contains("salesrep")
                        && !lcReqPath.Contains("/mystats")
                        && !lcReqPath.Contains("/managesettings/m1fixemail")
                        && !lcReqPath.Contains("/corelogic/")
                        && !lcReqPath.Contains("/login")
                        && !lcReqPath.Contains("/script/")
                        && !lcReqPath.Contains("/customer")
                        && !lcReqPath.Contains("api")
                        && !lcReqPath.Contains("/loiform/edit")
                        && !lcReqPath.Contains("/login/updatepassword")
                        && !lcReqPath.Contains("/login/forgotpassword")
                        && !lcReqPath.Contains("/login/changepassword")
                        && !lcReqPath.Contains("/login/changepasswordlink")
                        && !lcReqPath.Contains("/scheduler/deactivator")
                        && !lcReqPath.Contains("/payroll/pdfgeneration")
                        && !lcReqPath.Contains("/payroll/dealerorfranchisepdf")
                        && !lcReqPath.Contains("emailinsidesalepaymentpdf")
                        && !lcReqPath.Contains("/payroll/insidesalerepreport")
                        && !lcReqPath.Contains("/sunnova/oauth_callback")
                        && !lcReqPath.Contains("/proposals/proposallist")
                        && !lcReqPath.Contains("/proposaltool/cashcontract")
                        && !lcReqPath.Contains("/proposaltool/createproposalpdf")
                        )
                        {
                            context.Response.Redirect("/Login");
                            return;
                        }
                    }
                }
                await next.Invoke();
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.AddHubs();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            RotativaConfiguration.Setup(env2);

        }
    }
}