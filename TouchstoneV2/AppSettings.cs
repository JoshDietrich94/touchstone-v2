﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TouchstoneV2
{
    public class AppSettings
    {
        public class SalesForce
        {
            public const string ClientID = "3MVG9nkapUnZB56HYVN0F0uKkdjyks0q57HMOETrImDw1O6EJGRIewSqOEsBzYdkdM7g_HHNS.vq9Z01e.kyK";
            public const string RefreshToken = "5Aep861xmCGsWdSHLAD85aVzQajLjA2khUlgmyvAawkOhbZIb4PyYvPYB1qlI1kG7DDyCT30EZujZH9O0nqfgAP";
        }

        public class Nrel
        {
            public const string Production = "https://developer.nrel.gov/api/pvwatts/v6.json";
            public const string Key = "cLnRe2XbQC1U7DbDoUrs6nDiOTZAStzRfSygcMnt";
        }

        public class Aurora
        {
            public const string Production = "https://api.aurorasolar.com";
            public const string Tenant = "14eb3724-e1fa-441d-82a1-554dcca79233";
            public const string Key = "bcef38d6-9174-4b09-a328-43bdcc43532b";
            public const string Secret = "1f072c44-6600-4e81-8e65-88c773d0625f";
        }

        public class Genability
        {
            public const string Production = "https://api.genability.com";
            public const string ID = "84a7ddf3-f445-4fd7-9b52-4dd859845242";
            public const string Key = "d42d1f1c-2034-4fe0-95e5-d7383a3fb434";
        }

        public class BambooHR
        {
            public const string Production = "https://api.bamboohr.com";
            public const string Key = "1c7ba41b6a57feb6569195602fb5e14a8241250a";
            public const string Secret = "b749583d-0722-4922-9874-83daaec11067";
            public const string TSSecret = "a44d3ea2-681e-455a-bcac-fccf37f402a5";
        }

        public class Paylocity
        {
            public const string Production = "https://api.paylocity.com/api";
            public const string ID = "e+qe6yCPP0i3aMnqa12xOC04NTg2MjQ0MTI5NDg5MTUyNTE5";
            public const string Secret = "ad7ArUh3b3rp8Lm8JjJHYDfkdF0kYepq/aXALYJO9XXzM0vOgraQWPoJVw091xhdo+BdCI5E9DyEjBnnnQXJ3A==";
            public const string XML = "<RSAKeyValue><Modulus>vrXQOChO/uKeOePFCOp72oM6CogRoVyThlgcctLESVTFs2K5BacMF8ytxdvVLTC3Jw/aj7X1qsyD8inFcCjRQTPWaUmsptINP8uzbqT4MizKFvgpf3lSdAPnfRyGrp2UXpOLsO7tMRfdHLZatfSx2rowMkoziki4hlfs9RzgcXD+rHfgSZRM5mvd+G99B9Zlsve6euRrbrlOwluJ9q3VwNa+k8ErYtL6me+LGHF9NMqCBh1dWUZa+eG7FF+Y0qF11Mujo929lUJtug7yDjgo5iTC5U7J5IeYKedUuxuoizzs6O/A1H3xvI8M7m/Lhk873RyWzyztC2UNlFAxXW6mEQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        }

        public class SunNova
        {
            public const string Staging = "https://api.sunnova.com/v1/staging";
            public const string Production = "https://sunnovaenergy.force.com/services/apexrest/data/v1.0";
            public const string TokenServer = "https://sunnovaenergy.force.com/services/oauth2/token";
            public const string AuthServer = "https://sunnovaenergy.force.com/services/oauth2/authorize";
            public const string Key = "3MVG9d3kx8wbPieF8OANBPBbQAXZUzw42rtk__OF.gduu9R7NFwx7T9g6PjD_DKNAdRlD0ojzo00SySRu0JuA";
            public const string Secret = "7CD4EB8662D5A152406BFC40113297EB2C19BEB11846CA71192541827291D63D";
        }

        public class Google
        {
            public const string Maps = "https://maps.googleapis.com";
            public const string Key = "AIzaSyA8wQxPMZgMiwUXnM3eZRb7OeUCMWMxuGM";
        }

        public class DocuSign
        {
            public const string ClientID = "2e6ea5b0-caa1-4474-acb2-b9b0042fc4ff";
            public const string AccountID = "b3eb8539-1a4e-4957-9c3d-d2ac70f5a728";
            public const string ImpersonatedGUID = "7f21d5e6-903d-4791-ac34-a0b5e1ae99ac";
            public const string AuthServer = "account.docusign.com";

            public const string Key = @"-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAn3OBQmEFUo7HhVo6EhW4+0bq41YcbswtUoOM3IXR/blgqpk+
SXHitjOMjq1o1urZy8mvNnf0O8C0se2hjnJLAdfa1Rnal3AreKbHxIFA/UFKvIg0
OhwrKWjryCcpIq6ffHGEKxn88CYCw+ZNHnFHJ2uZmCqPttvlNsucnUDZTYmLtKzK
mdj3wdZ4IvPV6oNf7U58wHVgfI3BigF3MI2ZuD/lOn9ChUbsqq8npJFxLqLkFk1p
e7TsRg5cTfzfZTiMdKuvnWuUmTtGIHWfUUpbY0NrQ1WibAWx1qrEsoqtFo/ZHdKB
vjKl/hAxTy5GQM0P4Mi/8748ABO1hvbuT87vLwIDAQABAoIBAAJlMU1meLZ/R5cE
vneUFPXNYGzu17mAXrPtHvoXFor5tG6Lo8hlqhKznopsJ3WHDKOHkZa/YY9eOzir
CbDCncbWz8ZAv6xjtnt3Aew9RtUnHHDNmP9AehOCrHwSvobzWZj8eZBOMSMIZyU7
OfBZokiA0Mtc5FDKZ9yVXXJtTXZhMwIituTdaj10ORnOnOmlwrmUCi2EtoO73AXK
zckkrJDEsXRtK0oFBqJYzbhlzu5fZo9DfcO37OOCjcev/t0jxPmDLyoX9qTxNIuA
82tGSbYZq+DOah9+Ys6msrHs3O1M2Y5XlUk6bFmTvNYFJdYh47K91bqy5vxIIvxL
r6ocWDECgYEAzxWLqpFYK9X+UCeuV7k/ykm3kuID4Kcq6/MLllcSt1IvITcXMQIN
Z0zh+IYOfGXEfDS+JgaWO2cXmjPZ1FsDlmNQJ3VWjw1PbbT8KVIoGhHZglBxRj89
xoKCU/d7cl/icEUF/akOin+E72pThPaZ9s8Ogn0HB9fshle7tIjKF/8CgYEAxR2T
h+UPAYWILrwHSdvMhhQ+b8T9IIvUh+5gNeSr+dEAMhroNRZzNS4aIrckWX/YDXpQ
dXV15zvwknQqcM5+x7WGhVqf8jLmY1oNssj+X+aSWa2EjqUK7coUloh37heYxkcl
SHRjRkTsqVGudQcu2fpQ73Ihgh49ejiMPEzuqNECgYB1E71OxbWPswCA2K5cskM6
XssGo/mFPX2qwFJAwSG3uyMiKD9YWcyyJmlrVH9EHVUk07zdSsQM+ZACgmYZBQvW
LnfWlyRE5EWPxM77nZNGiejrIAeqq36FiKueRUDpvQQMbTmh27541veVVAu8FJ+f
Rhjx9TXQXWc/3MImo19+iQKBgHK+YtEqr++M0deSsRN3lbk/M5FQtKErhM6UHwGA
WfQUi+6e5ylwJiSA78nhnpFS8Gv4FboqZDX1GHfIk6QTMI4Ex4f9VzpgEukm1O6M
PjeJOwxlC9MVlCUUC6QNUPMSXWaep79jsR6IpNNzrwCE6+sL4e1hHMxn2hW5sTiw
+AThAoGBAK7ODCSmzeAa4kGMSmDr+i4XjgJSfVhB7Wn3tJe9KX5X6/+N0F1gRmJp
mWz0vclehQMpuuj9Lk4ZopnUr69O0L97HHmQ9CkCMYNXPVrTLg0Xsxoah9VQUWgD
UZZ4g6FcTqrwDRyOOZAdv8AIIpWXhOofU/jiwTonjIegOuCYz823
-----END RSA PRIVATE KEY-----&#xA;";
            public const string TargetAccountID = "FALSE";
        }

        public class ConnectionStrings
        {
            public const string Touchstone = "Data Source=192.168.0.20;Initial Catalog=Touchstone;User ID=sa;Password=MITTENS1;Max Pool Size=500;Min Pool Size=15;Pooling=true;";
        }

    }
}
