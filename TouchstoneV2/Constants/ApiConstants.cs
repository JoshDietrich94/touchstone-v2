﻿namespace TouchstoneV2.Constants
{
    public class ApiConstants
    {
        public const string INFY_BASE_API_URL = "https://api.infyboard.com/";
        public const string INFY_INSTALLS_BASE_API_URL = "http://192.168.0.20:21499/";
        public const string INFY_SITE_SURVEY_BASE_API_URL = "http://192.168.0.20:21400/";

        public const string IMAGES_ENDPOINT = "api/Files/Download/";
        public const string PUSH_NOTIFICATION_ENDPOINT = "api/notifications";

        public const string GRANT_TYPE_REFRESH = "refresh_token";

        // sales force endpoints 
        public const string SALES_FORCE_BASE_API_URL = "https://infinityenergy123.my.salesforce.com/";
        public const string SALES_FORCE_AUTHORIZE_ACCESS_TOKEN_URL = "https://infinityenergy123.my.salesforce.com/services/oauth2/token";

        public const string SALES_FORCE_EXPORT = "services/apexrest/v1/job/infinity-solar";
    }
}