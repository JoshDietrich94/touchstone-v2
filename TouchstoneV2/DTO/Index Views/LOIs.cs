﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TouchstoneV2.DTO.IndexViews
{
    public class LOIs
    {
        public DateTime CreationStampDT { get; set; }
        public DateTime ReturnAppointmentDT { get; set; }
        public DateTime LastNoteDateDT { get; set; }

        public string ID { get; set; }
        public string CreationStamp
        {
            get { return CreationStampDT.ToString("MM-dd-yyyy"); }
            set { CreationStampDT = DateTime.TryParse(value, out DateTime DT) ? DT : new DateTime(); }
        }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string LeadSource { get; set; }
        public double ElapsedDays { get; set; }
        public string ReturnAppointment
        {
            get { return ReturnAppointmentDT.ToString("MM-dd HH:mm"); }
            set { ReturnAppointmentDT = DateTime.TryParse(value, out DateTime DT) ? DT : new DateTime(); }
        }
        public double HoursUntil { get; set; }
        public string LastNoteDate
        {
            get { return LastNoteDateDT.ToString("MM-dd-yyyy HH:mm"); }
            set { LastNoteDateDT = DateTime.TryParse(value, out DateTime DT) ? DT : new DateTime(); }
        }
        public string LastDealNote { get; set; }
        public bool Duplicate { get; set; }
    }
}
