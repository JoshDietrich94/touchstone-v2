﻿using System.ComponentModel.DataAnnotations;

namespace TouchstoneV2.DTO.User
{
    public class AuthUserDTO
    {
        [Required(ErrorMessage = "Please enter a username or email address")]
        public string Login_Name { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        public string Password { get; set; }
    }
}
