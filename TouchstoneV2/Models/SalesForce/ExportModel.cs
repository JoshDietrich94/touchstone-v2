﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TouchstoneV2.Models.SalesForce
{
    public class ExportModel
    {
        public string Body { get; set; }
    }
}
