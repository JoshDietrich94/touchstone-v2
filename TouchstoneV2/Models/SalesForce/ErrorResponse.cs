﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TouchstoneV2.Models.SalesForce
{
    public class ErrorResponse
    {
        public string Message { get; set; }

        public string ErrorCode { get; set; }
    }
}
