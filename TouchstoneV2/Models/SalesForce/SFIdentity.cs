﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;


namespace TouchstoneV2.Models.SalesForce
{
    public class SFIdentity
    {
        public static string AuthToken { get; set; }

        public static string ClientId => AppSettings.SalesForce.ClientID;

        public static string RefreshToken => AppSettings.SalesForce.RefreshToken;
    }
}
