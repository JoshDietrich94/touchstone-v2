﻿using System;
using System.Collections.Generic;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class Users
    {
        public string ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationStamp { get; set; }
        public DateTime LastLogin { get; set; }
        public string PartnerID { get; set; }

        public virtual IntegratedPartners Partner { get; set; }
    }
}
