﻿using System;
using System.Collections.Generic;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class IntegratedPartners
    {
        public IntegratedPartners()
        {
            Leads = new HashSet<Leads>();
            Users = new HashSet<Users>();
        }

        public string ID { get; set; }
        public string PartnerName { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string PrimaryColor { get; set; }
        public string SecondaryColor { get; set; }

        public virtual ICollection<Leads> Leads { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
