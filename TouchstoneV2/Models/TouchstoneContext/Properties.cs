﻿using System;
using System.Collections.Generic;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class Properties
    {
        public string ID { get; set; }
        public string LeadID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public virtual Leads Lead { get; set; }
    }
}
