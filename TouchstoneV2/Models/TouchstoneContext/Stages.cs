﻿using System;
using System.Collections.Generic;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class Stages
    {
        public Stages()
        {
            Leads = new HashSet<Leads>();
        }

        public string ID { get; set; }
        public string Label { get; set; }

        public virtual ICollection<Leads> Leads { get; set; }
    }
}
