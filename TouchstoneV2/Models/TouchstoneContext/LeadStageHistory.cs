﻿using System;
using System.Collections.Generic;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class LeadStageHistory
    {
        public string ID { get; set; }
        public string LeadID { get; set; }
        public string StageID { get; set; }
        public string CreationAccountID { get; set; }
        public DateTime CreationStamp { get; set; }

        public virtual Users CreationAccount { get; set; }
        public virtual Leads Lead { get; set; }
        public virtual Stages Stage { get; set; }
    }
}
