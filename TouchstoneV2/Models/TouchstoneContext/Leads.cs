﻿using System;
using System.Collections.Generic;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class Leads
    {
        public Leads()
        {
            Customers = new HashSet<Customers>();
            Properties = new HashSet<Properties>();
        }

        public string ID { get; set; }
        public string PartnerID { get; set; }
        public string MainStageID { get; set; }
        public DateTime CreationStamp { get; set; }

        public virtual Stages MainStage { get; set; }
        public virtual IntegratedPartners Partner { get; set; }
        public virtual ICollection<Customers> Customers { get; set; }
        public virtual ICollection<Properties> Properties { get; set; }
    }
}
