﻿using System;
using System.Collections.Generic;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class Customers
    {
        public string ID { get; set; }
        public string LeadID { get; set; }
        public string LoiFirstName { get; set; }
        public string LoiLastName { get; set; }
        public string TitleFirstName { get; set; }
        public string TitleLastName { get; set; }
        public string VerifiedFirstName { get; set; }
        public string VerifiedLastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactRole { get; set; }

        public virtual Leads Lead { get; set; }
    }
}
