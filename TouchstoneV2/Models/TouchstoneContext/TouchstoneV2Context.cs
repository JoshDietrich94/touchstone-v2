﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TouchstoneV2.Models.TouchstoneContext
{
    public partial class TouchstoneV2Context : DbContext
    {
        public TouchstoneV2Context()
        {
        }

        public TouchstoneV2Context(DbContextOptions<TouchstoneV2Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<IntegratedPartners> IntegratedPartners { get; set; }
        public virtual DbSet<LeadStageHistory> LeadStageHistory { get; set; }
        public virtual DbSet<Leads> Leads { get; set; }
        public virtual DbSet<Properties> Properties { get; set; }
        public virtual DbSet<Stages> Stages { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=192.168.0.20;Initial Catalog=TouchstoneV2;User ID=sa;Password=MITTENS1;Max Pool Size=500;Min Pool Size=15;Pooling=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customers>(entity =>
            {
                entity.Property(e => e.ID)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContactRole)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Email)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.LeadID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LoiFirstName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.LoiLastName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TitleFirstName)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.TitleLastName)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.VerifiedFirstName)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.VerifiedLastName)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.LeadID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Customers_Customers");
            });

            modelBuilder.Entity<IntegratedPartners>(entity =>
            {
                entity.Property(e => e.ID)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Name1)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Name2)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryColor)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.SecondaryColor)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<LeadStageHistory>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CreationAccountID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreationStamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LeadID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.StageID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.CreationAccount)
                    .WithMany()
                    .HasForeignKey(d => d.CreationAccountID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeadStageHistory_Users");

                entity.HasOne(d => d.Lead)
                    .WithMany()
                    .HasForeignKey(d => d.LeadID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeadStageHistory_Leads");

                entity.HasOne(d => d.Stage)
                    .WithMany()
                    .HasForeignKey(d => d.StageID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeadStageHistory_Stages");
            });

            modelBuilder.Entity<Leads>(entity =>
            {
                entity.Property(e => e.ID)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreationStamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MainStageID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('10d93dc0-0d47-4cd8-bbe3-60c682390d6f')");

                entity.Property(e => e.PartnerID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('398dc54e-548d-45a3-8249-f326b2393f2e')");

                entity.HasOne(d => d.MainStage)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.MainStageID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leads_Stages");

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.PartnerID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leads_IntegratedPartners");
            });

            modelBuilder.Entity<Properties>(entity =>
            {
                entity.Property(e => e.ID)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.LeadID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.Properties)
                    .HasForeignKey(d => d.LeadID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Propertiers_Leads");
            });

            modelBuilder.Entity<Stages>(entity =>
            {
                entity.Property(e => e.ID)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.ID)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreationStamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerID)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.PartnerID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_IntegratedPartners");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
