﻿var connection = new signalR.HubConnectionBuilder().withUrl("/alertHub").build();

connection.start().then(function () {
    $(".customNotif").click(function (e) {
        e.preventDefault();
        var custMessage = window.prompt("Enter custom alert:");
        connection.invoke("SendMessage", custMessage).catch(function (err) {
            return console.error(err.toString());
        });
    });
}).catch(function (err) {
    return console.error(err.toString());
});

connection.on("ReceiveMessage", function (message) {
    alert(message);
});