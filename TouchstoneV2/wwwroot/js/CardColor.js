﻿function ChangePrimary(BgColor) {
    console.log(BgColor);
    $('.card-primary:not(.card-outline) > .card-header').css('background-color', `#${BgColor} !important`);
    $('.sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active').css('background-color', `#${BgColor} !important`);
}