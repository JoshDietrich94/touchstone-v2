﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouchstoneV2.Http;

namespace TouchstoneV2.Extensions.Infrastructure
{
    public static class DIServicesExtensions
    {
        public static IServiceCollection AddTouchstoneServices(this IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IHttpClientService, HttpClientService>();


            return services;
        }
    }
}
