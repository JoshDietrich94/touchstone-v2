﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using TouchstoneV2.Hubs.General;

namespace TouchstoneV2.Extensions.Infrastructure
{
    public static class HubRouteExtensions
    {
        public static IEndpointRouteBuilder AddHubs(this IEndpointRouteBuilder endpoints)
        {
            // general
            endpoints.MapHub<AlertHub>("/alertHub");

            return endpoints;
        }
    }
}
